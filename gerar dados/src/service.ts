let fs = require('fs');

export let AppService = () => {
    let dados = []
    let dia, mes, ano = 2022, hora, minutos, segundos;
    let semaforo
    let pacote = {}
    for (let i = 0; i<100000; i++){
      semaforo = Math.floor(Math.random() * 4)+1
      hora = Math.floor(Math.random() * 24);
      minutos = Math.floor(Math.random() * 60);
      segundos = Math.floor(Math.random() * 60);
     mes = Math.floor(Math.random() * 12)+1;
     if (mes in [1,3,5,7,8,10,12]) {
      dia = Math.floor(Math.random() * 31)+1;
     }else if (mes == 2) {
      dia = Math.floor(Math.random() * 28)+1;
     } else {
      dia = Math.floor(Math.random() * 30)+1;
     }
     pacote = {
      data: ano+'-'+mes+'-'+dia,
      hora: hora+':'+minutos+':'+segundos,
      semaforo: semaforo
     }
     dados.push(pacote)
     
     //console.log(pacote);
     
    }
    fs.writeFile("./dados.txt", JSON.stringify(dados), function(err){
      
      if(err){
      return console.log('erro')
    }
    console.log('Arquivo Criado');
  });
}
