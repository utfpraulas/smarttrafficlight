import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TrafficsModule } from './traffic/traffic.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [TrafficsModule],
  controllers: [
    AppController,
  ],
  providers: [AppService],
})
export class AppModule {}


