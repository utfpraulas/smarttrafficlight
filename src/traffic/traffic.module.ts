import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Traffic } from './traffic.entity';
import { TrafficsService } from './traffic.service';


@Module({
  imports: [TypeOrmModule.forFeature([Traffic]),],
  providers: [TrafficsService],
})
export class TrafficsModule {}
