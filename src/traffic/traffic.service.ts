import { Injectable } from '@nestjs/common';


let saiu = 0;
@Injectable()
export class TrafficsService {
  constructor(
  ) { }

  async ordenavetor (vet):  Promise<any> {
    let i, j, min, x;
    for (i=1; i<=vet.length-1; i++){
        min = i;
	  for (j=i+1; j<=vet.length; j++){
      if (vet[j].hora < vet[min].hora)
	      min = j;
	    }
	    x = vet[min];
	    vet[min] = vet[i];
	    vet[i] = x;
    }
  }

  async normalizare(dados): Promise<any> {
    let vetor = [[],[],[],[]]
    //criar matrix com vetores por vias [4][1000]
    for(let dado of dados){
      vetor[dado.semaforo].push(dado);
      this.ordenavetor(vetor[dado.semaforo]);
    }
    //classificar numero de carros por periodos
   
  }
  async validadados(id: string, form): Promise<any> {
    //baseado em uma saida de veiculos fixa(10 segundos sai 3 carros quando abertos), encontrar qual a sequencia de dados que deixara o fluxo mais otimizado,
    // vendo que o semaforo nao pode ficar mais de 60 seg vermelho e nem verde, e no minimo 10 seg verde ou vermelho,
    // de modo que seja alternado entre os 4 semaforos 
   
  }


/////////////////////////////////////////////////////////////////////////
async initDFS(nodos, inicio, fim){
    let i = inicio;
    console.log(nodos[i]);
    this.DFS(nodos, i, fim);
};

async DFS(nodos, i, fim) {
    nodos[i].visita = 2;
    if (nodos[i].relIdObj == fim) {
        console.log(nodos[i]);
        saiu = 1;
    };
    if (nodos[i].relIdObj != fim && saiu == 0) {
        for (var j = 0; j < nodos[i].filhosObj.length; j++) {
            let nodo = nodos[i].filhosObj[j].idVertice2;
            if (nodo == fim && saiu == 0) {
                console.log(nodos[nodo]);
                saiu = 1;
            };
            if (nodos[nodo].visita != 2 && saiu == 0) {
                console.log(nodos[nodo]);
                this.DFS(nodos, nodos[nodo].relIdObj, fim);
            };
        };
    };
};



}
